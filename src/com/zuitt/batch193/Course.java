package com.zuitt.batch193;

import java.util.ArrayList;

public class Course {
    // properties
    private String name;
    private String description;
    private int seats;
    private double fee;
    private String startDate;
    private String endDate;
    private User instructor;
    private ArrayList<String> enrollees = new ArrayList<>();
    // constructors
    // default
    public Course(){
        this.instructor = new User("Tee Jae", "Calinao", 25, "Antipolo City");
    };
    // parameterized
    public Course(String name, String description, int seats, double fee, String startDate, String endDate){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    // getters
    public String getName() { return name; }
    public String getDescription() { return description; }
    public int getSeats() { return seats; }
    public double getFee() { return fee; }
    public String getStartDate() { return startDate; }
    public String getEndDate() { return endDate; }
    public String getInstructorFirstName() { return this.instructor.getFirstName(); }
    public ArrayList<String> getEnrollees() {
        return enrollees;
    }

    // setters
    public void setName(String name) { this.name = name; }
    public void setDescription(String description) { this.description = description; }
    public void setSeats(int seats) { this.seats = seats; }
    public void setFee(double fee) { this.fee = fee; }
    public void setStartDate(String startDate) { this.startDate = startDate; }
    public void setEndDate(String endDate) { this.endDate = endDate; }
    // methods
    public void courseDetails(){
        System.out.println("Course name:");
        System.out.println(getName());
        System.out.println("Course description:");
        System.out.println(getDescription());
        System.out.println("Course seats:");
        System.out.println(getDescription());
        System.out.println("Course Instructor's first name:");
        System.out.println(getInstructorFirstName());
    }
    public void addEnrollee(String name){
        enrollees.add(name);
    }
    public void printEnrollees() {
        if (enrollees.isEmpty()){
            System.out.println("This course has no enrollees yet.");
        } else {
            for(int i = 0; i < enrollees.toArray().length; i++){
                System.out.println(enrollees.toArray()[i]);
            }
        }
    }
}
